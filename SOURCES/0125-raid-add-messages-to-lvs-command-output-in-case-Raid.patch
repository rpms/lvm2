From 1c773b76965efe1fd7c4b3c8211296ede5b4c1e5 Mon Sep 17 00:00:00 2001
From: Heinz Mauelshagen <heinzm@redhat.com>
Date: Wed, 6 Dec 2023 12:58:14 +0100
Subject: [PATCH 08/12] raid: add messages to lvs command output in case
 RaidLVs require a refresh

If a RaidLV mapping is required to be refreshed as a result of temporarily failed
and recurred RAID leg device (pairs) caused by writes to the LV during failure,
the requirement is reported by volume health character r' in position 9 of the
LV's attribute field (see 'man lvs' about additional volume health characters).

As this character can be overlooked, this patch adds messages to the top
of the lvs command output informing the user explicitely about the fact.

(cherry picked from commit b69f73b13ee55ef3feb7427c6dc099dc3472d9fc)
---
 tools/reporter.c | 11 +++++++++++
 1 file changed, 11 insertions(+)

diff --git a/tools/reporter.c b/tools/reporter.c
index eafba4fec..ff30a31a9 100644
--- a/tools/reporter.c
+++ b/tools/reporter.c
@@ -150,6 +150,13 @@ static int _check_merging_origin(const struct logical_volume *lv,
 	return 1;
 }
 
+static void _cond_warn_raid_volume_health(struct cmd_context *cmd, const struct logical_volume *lv)
+{
+	if (lv_is_raid(lv) && !lv_raid_healthy(lv) && !lv_is_partial(lv))
+		log_warn("WARNING: RaidLV %s needs to be refreshed!  See character 'r' at position 9 in the RaidLV's attributes%s.", display_lvname(lv),
+			 arg_is_set(cmd, all_ARG) ? " and its SubLV(s)" : " and also its SubLV(s) with option '-a'");
+}
+
 static int _do_lvs_with_info_and_status_single(struct cmd_context *cmd,
 					       const struct logical_volume *lv,
 					       int do_info, int do_status,
@@ -176,6 +183,8 @@ static int _do_lvs_with_info_and_status_single(struct cmd_context *cmd,
 			lv = lv->snapshot->lv;
 	}
 
+	_cond_warn_raid_volume_health(cmd, lv);
+
 	if (!report_object(sh ? : handle->custom_handle, sh != NULL,
 			   lv->vg, lv, NULL, NULL, NULL, &status, NULL))
 		goto out;
@@ -238,6 +247,8 @@ static int _do_segs_with_info_and_status_single(struct cmd_context *cmd,
 			seg = seg->lv->snapshot;
 	}
 
+	_cond_warn_raid_volume_health(cmd, seg->lv);
+
 	if (!report_object(sh ? : handle->custom_handle, sh != NULL,
 			   seg->lv->vg, seg->lv, NULL, seg, NULL, &status, NULL))
 		goto_out;
-- 
2.43.0

